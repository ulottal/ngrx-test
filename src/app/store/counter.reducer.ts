import { Action } from '@ngrx/store';

export const initialState = 0;

export function counterReducer(counter = initialState, action: Action) {
  switch (action.type) {
    case 'up':
      return counter + 1;

    case 'down':
      return counter - 1;

    case 'reset':
      return 0;

    default:
      return counter;
  }
}