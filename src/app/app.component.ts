import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  counter: number;

  constructor(private store: Store<any>) {
    store.pipe(select('counter')).subscribe(counter => this.counter = counter);
  }
}
